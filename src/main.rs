use rouille::start_server;
use rouille::input::json_input;
use gitlab::webhooks::MergeRequestHook;
use std::thread;
use gl_bot::process_hook_data;
use config::{Config, File};

fn main() {
    let mut conf = Config::new();
    let str_error_handler = |e| {
        panic!("Error in the config content ({}).", e);
    };

    let conf = conf.merge(File::with_name("settings.toml")).unwrap_or_else(|e| {
        panic!("Error loading the config. {}", e);
    });

    let port = conf.get_int("port").unwrap_or(80);
    let watcher_conf = gl_bot::WatcherConfig {
        token: conf.get_str("token").unwrap_or_else(str_error_handler),
        host: conf.get_str("gitlab_host").unwrap_or_else(str_error_handler),
        insecure: conf.get_bool("insecure").unwrap_or(false)
    };

    start_server(format!("0.0.0.0:{}", port), move | request | {

        // We need to clone the config to pass it to process_hook_data as the
        // spawned thread might (theoretically) outlive this closure.
        let conf = (&watcher_conf).clone();
        let result = json_input::<MergeRequestHook>(request);
        match result {
            Ok(info) => {
                thread::spawn(move || {
                    process_hook_data(info, &conf)
                });
                rouille::Response::empty_204()
            },
            Err(error) => {
                println!("{}", error);
                rouille::Response::empty_400()
            }
        }
    });
}
