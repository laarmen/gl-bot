use gitlab::webhooks::MergeRequestHook;
use gitlab::QueryParamSlice;

macro_rules! query_param_slice {
    ( $( $x:expr ),* ) => (
        &[$($x),*] as QueryParamSlice
    )
}

#[derive(Debug,Clone)]
pub struct WatcherConfig {
    pub host: String,
    pub token: String,
    pub insecure: bool
}

pub fn process_hook_data(mr_hook: MergeRequestHook, config: &WatcherConfig) {
    let mut builder = gitlab::GitlabBuilder::new(&config.host, &config.token);
    let gl = if config.insecure {builder.insecure()} else { &mut builder }.build().expect("Couldn't connect to Gitlab, fatal error!");

    let info = &mr_hook.object_attributes;
    let project = &mr_hook.project;
    if info.work_in_progress {
        println!("Ignoring {}!{} because still WIP", project.path_with_namespace, info.iid);
        return;
    }
    println!("Processing {}!{}", project.path_with_namespace, info.iid);
    let mr = gl.merge_request(info.target_project_id, info.iid, query_param_slice![]);
    if let Err(err) = mr {
        println!("Something went wrong: {}", err);
        return;
    }
    let mr = mr.unwrap();
    let results = gl.merge_request_notes(info.target_project_id, info.iid, query_param_slice![]);
    println!("MR: {}, results: {:?}", mr.project_id, results.unwrap());
}
